#!/usr/bin/python3

import unittest
import numbers
from src.calculator import cal, add, subtract, divide

class TestCalculator(unittest.TestCase):
    '''Testing the calculator'''

    def setUp(self):
        '''Set up testing objects'''
        self.a = 439
        self.b = 4

    def test_add(self):
        '''Testing add menthod'''
        calculator = cal(self.a, self.b)
        self.assertEqual(calculator.add(),443)

    def test_subtract(self):
        '''Testing subtract method'''
        calculator = cal(self.a, self.b)
        self.assertEqual(calculator.sub(), 435)



numberical_list = [1,2,3]
mixed_types_list = [1,'test',3]

class TestAdd(unittest.TestCase):
    def test_add_function_numerical(self):
        self.assertEqual(add(numberical_list), 6)

    def test_add_function_mixed_types(self):
        self.assertEqual(add(mixed_types_list), 4)

class TestSubstract(unittest.TestCase):
    def test_substract_function_numerical(self):
        self.assertEqual(subtract(numberical_list), -6)

    def test_substract_function_mixed_types(self):
        self.assertEqual(subtract(mixed_types_list), -4)

class TestEqualLength(unittest.TestCase):
    def test_equal_lists(self):
        self.assertListEqual([1,2,3], [1,2,3], "List are different")

    def test_first_arg_is_in_second_arg(self):
        self.assertIn([1,2,3], ([1,2,3],5,9, 'unit'), "List 1 is not in List 2")



j = 5
q = 8
z = 0
u = "unit"

class TestDivide(unittest.TestCase):
    def test_divide_functionl(self):
        self.assertTrue(isinstance(divide(j,q), numbers.Number))

    def test_divide_function_by_zero(self):
        self.assertIsNone(divide(u, z), "unaccepted inputs")


if __name__ == '__main__':
    unittest.main()
