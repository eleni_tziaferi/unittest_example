# Unit Testing with Python

## Running the unit tests
You can run the unit tests with the following command
```
python tests/unit_tests.py
```

`unit_tests.py` imports the calculator that we created and the `unittest` module needed to set up and run the tests.

## Add more unit tests
You should add more test cases to the `unit_tests.py` file. The general structure is to have a class that takes in the unittest module: `class TestCalculator(unittest.TestCase)`. Then, add functions that test a specific part of the code written in calculator.py

### Testing the class
In order to test a class in Python, you'll simply import it and then have a setup function which will create the object. In our case, we set self.a and self.b to two different numbers.

You should test for subtraction, division and multiplication.

### Testing a function
There is also an addition function in `src/calculator.py`. This function takes in an list of items (currently, it does not check to see if they're numbers) and then returns the sum.
